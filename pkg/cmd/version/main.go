// Copyright © 2017 The Qaclana Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package version holds the command that exposes the version number. Based on the similar
// feature present on Jaeger.
package version

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
)

var (
	// commitFromGit is a constant representing the source version that
	// generated this build. It should be set during build via -ldflags.
	commitSHA string
	// build date in ISO8601 format, output of $(date -u +'%Y-%m-%dT%H:%M:%SZ')
	date string
)

// Info holds build information
type Info struct {
	GitCommit string `json:"commit"`
	BuildDate string `json:"date"`
}

// Get creates and initialized Info object
func Get() Info {
	return Info{
		GitCommit: commitSHA,
		BuildDate: date,
	}
}

// NewVersionCommand creates the command that exposes the version
func NewVersionCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Print the version",
		Long:  `Print the version and build information`,
		RunE: func(cmd *cobra.Command, args []string) error {
			info := Get()
			json, err := json.Marshal(info)
			if err != nil {
				return err
			}
			fmt.Println(string(json))

			return nil
		},
	}
}
