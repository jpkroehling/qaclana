// Copyright © 2017 The Qaclana Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package thehoneypotproject specifies a processor that matches the client IP with thehoneypotproject
package thehoneypotproject

import (
	"fmt"
	"net"
	"net/http"
	"strings"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/spf13/viper"

	"gitlab.com/qaclana/qaclana/pkg/processor"
)

// Processor is the thehoneypotproject's processor implementation
type Processor struct {
}

// New creates a new instance of the Honeypot Project processor
func New() *Processor {
	return &Processor{}
}

// Process an incoming request, checking it's remote address against the honeypot project's list
func (p *Processor) Process(req *http.Request) (processor.Outcome, error) {
	sp, _ := opentracing.StartSpanFromContext(req.Context(), "processor-thehoneypotproject")
	defer sp.Finish()

	ip, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		// let's try to parse it as if it's a IP-only string:
		ip = req.RemoteAddr
	}

	client := net.ParseIP(ip)
	if client == nil {
		sp.SetTag("error", true)
		sp.SetTag("processor-outcome", "NEUTRAL")
		sp.LogKV("error", "IP couldn't be parsed", "ip", ip)
		return processor.NEUTRAL, fmt.Errorf("thehoneypotproject: %s could not be parsed into an IP", ip)
	}

	// No support for IPv6 yet on The Honeypot Project
	if client.To4() == nil {
		sp.SetTag("processor-outcome", "NEUTRAL")
		sp.LogKV("success", "IPv6 is never blocked")
		return processor.NEUTRAL, nil
	}

	apiKey := viper.GetString("thehoneypot-apikey")
	if apiKey == "" {
		sp.SetTag("warning", true)
		sp.SetTag("processor-outcome", "NEUTRAL")
		sp.LogKV("warning", "Not configured")
		return processor.NEUTRAL, fmt.Errorf("thehoneypotproject is not configured, skipping")
	}

	address := fmt.Sprintf("%s.%s.dnsbl.httpbl.org", apiKey, reversedIP(client.String()))

	ips, err := net.LookupIP(address)
	if len(ips) == 0 {
		// should have returned a "no such host" (NXDOMAIN), but there's no cheap way to check it, it seems!
		sp.SetTag("processor-outcome", "NEUTRAL")
		sp.LogKV("success", "IP not blocked", "lookup-result", err)
		return processor.NEUTRAL, nil
	}

	// we got results, and this is a bad thing: http://www.projecthoneypot.org/httpbl_api.php
	sp.SetTag("processor-outcome", "BLOCKED")
	sp.LogKV("success", "IP is blocked", "lookup-result", ips[0].String())
	return processor.BLOCK, nil
}

func reversedIP(c string) string {
	splitted := strings.Split(c, ".")
	return fmt.Sprintf("%s.%s.%s.%s", splitted[3], splitted[2], splitted[1], splitted[0])
}

func (p *Processor) String() string {
	return "The Honeypot Project"
}
